import { createSelector } from '@ngrx/store';
import { ProductState } from 'src/app/models/product.model';
import { AppState } from '../app.state';


export const selectProducts = 
    (state: AppState) => state.products;

export const selectProductsList = createSelector(
    selectProducts,
    (state: ProductState) => state.products
);

export const selectProductsLoading = createSelector(
    selectProducts,
    (state: ProductState) => state.loading
);