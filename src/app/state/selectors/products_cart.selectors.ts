import { createSelector } from '@ngrx/store';
import { AppState } from '../app.state';
import { ProductsCartState } from '../../models/cart.model';


export const selectProductsCartState = 
    (state: AppState) => state.productsCart;

export const selectProductsCartList = createSelector(
    selectProductsCartState,
    (state: ProductsCartState) => state.items
)

export const selectProductsCartNumberProducts = createSelector(
    selectProductsCartList,
    (state) =>  state.reduce( 
        (total,curr) => total + Number(curr.quantity) , 0 ));

export const selectProductsCartTotalPrice = createSelector(
    selectProductsCartList,
    (state) =>  state.reduce( 
        (total,curr) => total + Number(curr.product.price * curr.quantity) , 0 ));






