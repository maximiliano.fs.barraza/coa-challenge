import { ActionReducerMap } from '@ngrx/store';
import { ProductState } from 'src/app/models/product.model';
import { productsReducer } from './reducers/products.reducers';
import { ProductsCartState } from '../models/cart.model';
import { productsCartReducer } from './reducers/products_cart.reducer';

//ESTA INTERFAZ PASARLA A OTRA Y METERLA DENTRO DE APP STATE
//Products Interface
//Cart interface
//ETC...
export interface AppState {
    products: ProductState
    productsCart: ProductsCartState
}

export const ROOT_REDUCERS:ActionReducerMap<AppState> = {
    products: productsReducer,
    productsCart: productsCartReducer
    
}