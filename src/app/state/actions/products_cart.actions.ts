import { createAction, props } from '@ngrx/store';
import { ProductsCart } from '../../models/cart.model';

export const loadingProductsCart = createAction(
    '[CartsProducts] Load Cart Products',
    props<{ id: string }>()
);

export const loadProductsCartSuccess = createAction(
    '[CartsProducts] Load Cart Products Success',
    props<{ items: ProductsCart[] }>()
)

