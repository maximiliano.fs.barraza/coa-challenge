import { createAction, props } from '@ngrx/store';
import { Product } from '../../models/product.model';

export const loadingItems = createAction(
    '[Products] Load Products'
);

export const loadItemsSuccess = createAction(
    '[Products] Load Products Success',
    props<{ products: Product[] }>()
)

