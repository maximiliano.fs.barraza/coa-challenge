import { createReducer, on } from '@ngrx/store';
import { loadingItems, loadItemsSuccess } from '../actions/product.actions';
import { Product } from 'src/app/models/product.model';
import { ProductState } from '../../models/product.model';


export const initialState:ProductState = {
    products: [],
    loading: false,
};

export const productsReducer = createReducer(
    initialState,
    on(loadingItems, (state)=>{
        return { ...state, loading: true }
    }),
    on(loadItemsSuccess, (state, {products})=>{
        return { ...state, loading: false, products }

    })
);