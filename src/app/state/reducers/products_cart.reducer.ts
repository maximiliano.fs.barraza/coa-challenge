import { createReducer, on } from '@ngrx/store';
import { ProductsCartState } from '../../models/cart.model';
import { loadingProductsCart, loadProductsCartSuccess } from '../actions/products_cart.actions';


export const initialState:ProductsCartState = {
    items: []
};

export const productsCartReducer = createReducer(
    initialState,
    on(loadingProductsCart, (state)=>{
        return { ...state, loading: true }
    }),
    on(loadProductsCartSuccess, (state, {items})=>{
        return { ...state, loading: false, items }
    })
);