import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, debounceTime } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ProductService } from '../../services/product.service';


@Injectable()
export class ProductEffects {
    loadProducts$ = createEffect(
        () => this.actions$.pipe(
            ofType('[Products] Load Products'),
            mergeMap(() => 
                this.productService.getAll().pipe(
                    debounceTime(1000),
                    map(products => ({   
                        type: '[Products] Load Products Success',
                        products
                    })),
                    catchError(error => EMPTY)
                )
            )
        )
    )

    constructor(    private productService:ProductService,
                    private actions$: Actions) 
    {}

}