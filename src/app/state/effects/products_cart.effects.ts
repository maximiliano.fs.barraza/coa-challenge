import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, debounceTime } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { CartService } from '../../services/cart.service';

@Injectable()
export class ProductsCartEffects {
    loadCartProducts$ = createEffect(
        () => this.actions$.pipe(
            ofType('[CartsProducts] Load Cart Products'),
            mergeMap(
                (data:any) => {
                    return this.cartService.getAllProducts(data.id).pipe(
                        debounceTime(1000),
                        map(items =>({   
                            type: '[CartsProducts] Load Cart Products Success',
                            items,
                        })),
                        catchError(error => EMPTY)
                    )
                }
                
            )
        )
    )

    constructor(    private cartService:CartService,
                    private actions$: Actions) 
    {}
}