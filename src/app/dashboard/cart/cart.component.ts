import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../state/app.state';
import { loadingProductsCart } from '../../state/actions/products_cart.actions';
import { Observable, debounceTime, take, BehaviorSubject, Subject, merge, combineLatest } from 'rxjs';
import { ProductActionsComponent } from '../../components/product-actions/product-actions.component';
import { ColDef, ColGroupDef } from 'ag-grid-community';
import { CartService } from '../../services/cart.service';
import { CartActionsComponent } from '../../components/cart-actions/cart-actions.component';
import { map } from 'rxjs/operators';
import { selectProductsCartList, selectProductsCartNumberProducts, selectProductsCartState, selectProductsCartTotalPrice } from '../../state/selectors/products_cart.selectors';
import { ProductsCart } from '../../models/cart.model';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {

  cartID:string = '';

  public columnDefsProducts: (ColDef | ColGroupDef)[] = [
    { field: 'product.name', 
      resizable: true,
      sortable: true,
      headerName: 'Name',
      minWidth:130, 
      maxWidth:170,
      flex:1
    },
    { field: 'product.price',
      resizable: true,
      sortable: true,
      headerName: 'Price',
      minWidth:80, 
      maxWidth:100,
      flex:1
    },
    { field: 'quantity',
      resizable: true,
      sortable: true,
      headerName: 'Quantity',
      minWidth:80, 
      maxWidth:100,
      flex:1
    },
    { field: 'actions',
      resizable: false,
      headerName: 'Actions',
      minWidth:150, 
      maxWidth:160,
      cellRenderer: CartActionsComponent,
    },
  ];

  public countProducts$: Observable<number> = new Observable()
  public price$:Observable<number> = new Observable();
  public items$: Observable<any> = new Observable();

  constructor(  private store: Store<AppState>,
                private cartService:CartService) {}

  ngOnInit(): void {
    this.cartID = localStorage.getItem('currentCart') || "";
    this.store.dispatch(loadingProductsCart({id: this.cartID}))
    this.items$ = this.store.select(selectProductsCartList)
    this.countProducts$ = this.store.select(selectProductsCartNumberProducts);
    this.price$ = this.store.select(selectProductsCartTotalPrice)

    
  }

  showCompleteCardWarning(){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, complete Cart!'
    }).then((result) => {
      if (result.isConfirmed) {
          this.completeCart()
          Swal.fire(
            'Completed!',
            'Cart has been marked as completed, you has a new cart to add products',
            'success'
          )

      }
    })
  }

  completeCart(){
    const userID:string = localStorage.getItem('userID') || "";
    this.cartService.completeCart(this.cartID).then(()=>{
      this.cartService.createNewCart().then(cart=>{
        this.cartService.getUserCartDocID(userID).pipe(take(1)).subscribe(userCartDocID=>{
          this.cartService.updateUserCart(userCartDocID[0], cart.id).then(()=>{
            const previousCartID = this.cartID;
            localStorage.setItem('currentCart', cart.id);
            this.cartID = localStorage.getItem('currentCart') || "";
            this.store.dispatch(loadingProductsCart({id: cart.id}))
            this._removeProductsAfterCompleteCart(previousCartID);
          })
        })
      })
    })
  }

  private _removeProductsAfterCompleteCart(cartID:string){
    this.cartService.removeProductsAfterCompleteCart(cartID).subscribe((querySnapshot)=>{
      let products= querySnapshot.docs.map((doc) => {
        return { id: doc.id }
      })
      products.forEach(product=>{
        this.cartService.removeProductFromCart(product.id)
      })
    })
  }

}
