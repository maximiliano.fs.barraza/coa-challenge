import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ColDef } from 'ag-grid-community';
import { Observable, take } from 'rxjs';
import { loadingItems } from '../../state/actions/product.actions';
import { Product } from 'src/app/models/product.model';

import { selectProductsList } from '../../state/selectors/product.selectors';
import { AppState } from '../../state/app.state';
import { ProductActionsComponent } from 'src/app/components/product-actions/product-actions.component';
import { MatDialog } from '@angular/material/dialog';
import { ProductsFormComponent } from '../../components/products-form/products-form.component';
import { CartService } from '../../services/cart.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  public products$: Observable<any> = new Observable()
  productSelected$: Observable<Product> = new Observable();


  public columnDefsProducts: ColDef[] = [
    { field: 'name', 
      resizable: true,
      sortable: true,
      headerName: 'Name',
      minWidth:130, 
      maxWidth:170,
      flex:1
    },
    { field: 'description',
      resizable: true,
      headerName: 'Description',
      minWidth:500, 
      maxWidth:600,
      wrapText: true,
      autoHeight: true,
      flex:2
    },
    { field: 'price',
      resizable: true,
      sortable: true,
      headerName: 'Price',
      minWidth:80, 
      maxWidth:100,
      flex:1
    },
    
    { field: 'actions',
      resizable: false,
      headerName: 'Actions',
      minWidth:150, 
      maxWidth:190,
      flex:1,
      cellRenderer: ProductActionsComponent,
    },
  ];

  public columnDefsCart:ColDef[]=[]

  constructor(private store: Store<AppState>,
              private cartService:CartService,
              private dialog: MatDialog) {}

  ngOnInit(): void {
    this.store.dispatch(loadingItems())
    this.products$ = this.store.select(selectProductsList);
    const userID = localStorage.getItem('userID');
    this.cartService.getUserCart(userID!).pipe(take(1)).subscribe(
      resp=>localStorage.setItem('currentCart', resp[0].cartID)
    )
  }

  openFormDialog(): void {
    this.dialog.open(ProductsFormComponent, {
      width: '350px',
    });
  }

}
