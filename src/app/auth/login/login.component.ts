import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        label: 'Email address',
        placeholder: 'Enter email',
        required: true,
        pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        label: 'Password',
        type: 'password',
        placeholder: 'Enter password',
        required: true,
      }
    }
  ];


  constructor(  private authService:AuthService,
                private afAuth:AngularFireAuth,
                private router:Router
            ) { }

  ngOnInit(): void {
  }


  onSubmit() {
    const user:string = this.form.get('email')?.value || "";
    const password:string = this.form.get('password')?.value || "";
    this.authService.login(user, password).then( resp=>{
      if(resp.user?.emailVerified){
        this.router.navigate(['/dashboard'])
        localStorage.setItem('userID',resp.user.uid)
        this.authService.isLoggedIn=true;
      }
      else{
        console.log('CORREO INVALIDO')
      }
    }).catch(error=>console.log(error))
  }
}
