import { Component, OnInit } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormControl, FormGroup, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { CartService } from '../../services/cart.service';
import { AuthService } from '../../services/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({});
  fields: FormlyFieldConfig[] = [
    {
      validators: {
        validation: [
          { name: 'fieldMatch', options: { errorPath: 'passwordConfirm' } },
        ],
      },
      fieldGroup: [
        {
          key: 'email',
          type: 'input',
          templateOptions: {
            label: 'Email address',
            placeholder: 'Enter email',
            required: true,
          }
        },
        {
          key: 'password',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'Password',
            placeholder: 'Must be at least 6 characters',
            required: true,
            minLength: 6,
          },
        },
        {
          key: 'passwordConfirm',
          type: 'input',
          templateOptions: {
            type: 'password',
            label: 'Confirm Password',
            placeholder: 'Please re-enter your password',
            required: true,
          },
        },
      ],
    }
  ];

  constructor(
      private authService:AuthService,
      private cartService:CartService,
      private router:Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    const user = this.form.get('email')?.value || "";
    const password = this.form.get('password')?.value || "";
    this.authService.register(user, password).then(
      (resp)=>{
        Swal.fire({
          icon: 'success',
          title: 'Check your email to verify your account',
          showConfirmButton: false,
          timer: 1500
        })
        resp.user?.sendEmailVerification();
        this.router.navigate(['auth/login'])
        this.form.reset();
        if(resp.user){
          this.cartService.createNewCart().then(data=>{
            this.cartService.createNewUserCart(resp.user!.uid, data.id)
          })
        }
      }).catch(error=>{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
        })
      })
  }

  checkPasswords(control: FormControl): ValidationErrors | null {
    const password = this.form.get('password')?.value;
    console.log('asd');
    return password === control.value ? null : {notSame: true};
  }

}
