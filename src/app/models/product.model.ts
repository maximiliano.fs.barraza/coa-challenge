export interface Product {
    id?:string,
    name:string,
    price:number,
    description:string,
}

export interface ProductState {
    products: ReadonlyArray<Product>;
    loading: boolean;
}