import { Product } from './product.model';

export interface Cart{
    id:string;
    complete:boolean //Pending:0-Complete:1
}

export interface UserCart{
    userID:string,
    cartID:string,
}

export interface ProductsCart{
    cartID:string
    product: Product,
    quantity: number;
}


export interface ProductsCartState{
    items: ReadonlyArray<ProductsCart>
}
