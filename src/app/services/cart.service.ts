import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Product } from '../models/product.model';
import { map } from 'rxjs/operators';
import { ProductsCart, UserCart } from '../models/cart.model';
import { arrayUnion, increment } from "firebase/firestore";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private fireStore:AngularFirestore) { }

  createNewCart(){
    return this.fireStore.collection('carts').add(
      {
        complete:false
      }
    );
  }

  createNewUserCart(userID:string, cartID:string){
    return this.fireStore.collection('users_cart').add(
      {
        userID:userID,
        cartID:cartID,
      }
    )
  }

  getUserCartDocID(userID:string){
    return this.fireStore.collection('users_cart', ref => ref.where('userID', '==', userID)).snapshotChanges().pipe(
      map(data => {
        return data.map(e => {
          return e.payload.doc.id;
        })
      })
    )
  }

  updateUserCart(userCartDocID:string, cartID:string){
    return this.fireStore.collection('users_cart').doc(userCartDocID).update(
      {
        cartID:cartID
      }
    )
  }

  getUserCart(userID:string){
    return this.fireStore.collection('users_cart', ref => ref.where('userID', '==', userID)).snapshotChanges().pipe(
      map(data => {
        return data.map(e => {
          const userCart:UserCart = e.payload.doc.data() as UserCart;
          return userCart;
        })
      })
    )
  }

  getAllProducts(cartID:string){
    return this.fireStore.collection('products_cart', ref => ref.where('cartID', '==', cartID)).
    snapshotChanges()
    .pipe(
      map(data=>{
        return data.map(e=>{
          const productCart = e.payload.doc.data() as ProductsCart;
          return productCart;
        })
      })
    )
  }

  getProductDocID(cartID:string, product:Product){
    return this.fireStore.collection('products_cart', ref => ref.where('cartID', '==', cartID).where('product', '==', product)).snapshotChanges().pipe(
      map(data => {
        return data.map(e => {
          return e.payload.doc.id;
        })
      })
    )
  }

  createNewProductsCart(cartID:string, product: Product){
    return this.fireStore.collection('products_cart',ref => ref.where('cartID', '==', cartID)).add(
      {
        cartID:cartID,
        product: product,
        quantity:1
      }
    );
  }

  updateProductQuantity(productID:string, quantity:number){
    return this.fireStore.collection('products_cart').doc(productID).update(
      {
        quantity: increment(quantity)
      }
    )
  }

  removeProductFromCart(productID:string){
    return this.fireStore.collection('products_cart',).doc(productID).delete();
  }

  addProductToCart(cartID:string, product:Product){
    return this.fireStore.collection('products_cart').doc(cartID).update(
      {
        products: arrayUnion(product),
        quantity: increment(1)
      }
    )
  }

  completeCart(cartID:string){
    console.log(cartID);
    return this.fireStore.collection('carts').doc(cartID).update(
      {
        complete:true
      }
    )
  }

  removeProductsAfterCompleteCart(cartID:string){
    return this.fireStore.collection('products_cart', ref => ref.where('cartID', '==', cartID)).get();
  }
}
