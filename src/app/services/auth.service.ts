import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn:boolean = false;

  constructor(private afAuth:AngularFireAuth,
              private router:Router) { }

  login(user:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(user,password)
  }

  logout(){
    this.isLoggedIn=false;
    localStorage.clear()
    this.router.navigate(['/auth']);
  }

  register(user:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(user, password)
  }

  userAuthenticated():boolean{
    const userID = localStorage.getItem('userID');
    if(userID){
      return true;
    }
    return false;
  }
}
