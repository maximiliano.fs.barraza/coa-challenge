import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private fireStore:AngularFirestore) 
  { }

  getAll(){
    return this.fireStore.collection('products').snapshotChanges().pipe(
      map(data => {
        return data.map(e => {
          const product:Product = e.payload.doc.data() as Product;
          product.id = e.payload.doc.id;
          return product;
        })
      })
    );
  }

  addNewProduct(product:Product):Promise<any>{
    return this.fireStore.collection('products').add(product);
  }

  updateProduct(product_id:string, product:Product):Promise<any>{
    return this.fireStore.collection('products').doc(product_id).update(product)
  }

  deleteProduct(productID:string):Promise<any>{
    return this.fireStore.collection('products').doc(productID).delete();
  }

  
}
