import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { take, Observable } from 'rxjs';
import { ICellRendererParams } from 'ag-grid-community';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-cart-actions',
  templateUrl: './cart-actions.component.html',
  styleUrls: ['./cart-actions.component.sass']
})
export class CartActionsComponent implements OnInit {

  public params!: ICellRendererParams;

  constructor(  private cartService:CartService) { }

  ngOnInit(): void {
  }

  increaseQuantity(quantity:number){
    const productCartSelected = {...this.params.data}
    const productDB = this.cartService.getProductDocID(productCartSelected.cartID, productCartSelected.product).
      pipe(
        take(1)
      )
    if(quantity>0){
      productDB.subscribe(resp=>{
        this.cartService.updateProductQuantity(resp[0],quantity)
      })
    }else{
      if(productCartSelected.quantity>1){
        productDB.subscribe(resp=>{
          this.cartService.updateProductQuantity(resp[0],quantity)
        })
      }
    }
  }

  removeProduct(){
    const productCartSelected = {...this.params.data}
    const productDB = this.cartService.getProductDocID(productCartSelected.cartID, productCartSelected.product).
      pipe(
        take(1)
      );
    this._showRemoveDialog(productDB);
  }

  private _showRemoveDialog(productDB: Observable<string[]>){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        productDB.subscribe(resp=>{
          this.cartService.removeProductFromCart(resp[0]);
          Swal.fire(
            'Deleted!',
            'The selected product was removed from your cart',
            'success'
          )
        })
      }
    })
  }

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

}
