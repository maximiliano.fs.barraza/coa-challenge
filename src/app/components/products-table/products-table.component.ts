import { Component, Input} from '@angular/core';
import { Observable } from 'rxjs';
import { ColDef } from 'ag-grid-community';
import { Store } from '@ngrx/store';
import { AppState } from '../../state/app.state';
import { selectProductsLoading } from '../../state/selectors/product.selectors';
import { ProductsCart } from '../../models/cart.model';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
  styleUrls: ['./products-table.component.sass']
})
export class ProductsTableComponent {

  public defaultColDef: ColDef = {
    resizable: true,
  };
  
  @Input() items$: Observable<Product[] | ProductsCart[]> = new Observable()
  @Input() columnDefs: ColDef[] = []
  loading$: Observable<boolean>

  constructor(private store: Store<AppState>) {
    this.loading$ = this.store.select(selectProductsLoading)   
  }
}
