import { AfterViewInit, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from '../../services/product.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.sass']
})
export class ProductsFormComponent implements AfterViewInit {

  isOpenForEdit:boolean=false;
  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      templateOptions: {
        label: 'Name',
        placeholder: 'Enter a product name',
        required: true,
        minLength: 3,
        maxLength: 20
      },
    },
    {
      key: 'price',
      type: 'input',
      templateOptions: {
        label: 'Price',
        type: 'number',
        placeholder: 'Enter a price of the product',
        required: true,
        maxLength: 8,
        min: 0,
        max: 99999
      }
    },
    {
      key: 'description',
      type: 'textarea',
      templateOptions: {
        label: 'Description',
        type: 'description',
        placeholder: 'Enter a description of the product',
        required: true,
        minLength: 10,
        maxLength: 200,
      },
    },
    
  ];

  constructor(  private productService:ProductService,
                private changeDetectorRef: ChangeDetectorRef,
                private dialogRef: MatDialogRef<ProductsFormComponent>,
                @Inject(MAT_DIALOG_DATA) private data: {product: Product,
                },
              ) 
  { }


  ngAfterViewInit(): void {
    if(this.data){
      this.form.reset({
        name: this.data.product.name,
        price: this.data.product.price,
        description: this.data.product.description
      })
      this.changeDetectorRef.detectChanges()
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  saveProduct(){
    const name = this.form.get('name')?.value || null;
    const price = this.form.get('price')?.value || null;
    const description = this.form.get('description')?.value || null;
    if(name && price && description){
      const product:Product = {
        name,
        price,
        description
      }
      if(this.data){
        this.productService.updateProduct(this.data.product.id!, product);
        Swal.fire({
          icon: 'success',
          title: 'Product has been updated',
          showConfirmButton: false,
          timer: 1500
        })
      }else{
        this.productService.addNewProduct(product);
        Swal.fire({
          icon: 'success',
          title: 'New product has been saved',
          showConfirmButton: false,
          timer: 1500
        })
      }
      this.dialogRef.close();
    }
  }
}
