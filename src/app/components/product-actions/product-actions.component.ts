import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ProductService } from 'src/app/services/product.service';
import { ProductsFormComponent } from '../products-form/products-form.component';
import { MatDialog } from '@angular/material/dialog';
import { CartService } from '../../services/cart.service';
import { Product } from '../../models/product.model';
import { take } from 'rxjs';
import { ICellRendererParams } from 'ag-grid-community';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-product-actions',
  templateUrl: './product-actions.component.html',
  styleUrls: ['./product-actions.component.sass']
})
export class ProductActionsComponent implements ICellRendererAngularComp {

  private params!: ICellRendererParams;

  cartID:string

  productsCartID!:string;

  constructor(  private productService: ProductService,
                private cartService:CartService,
                private dialog: MatDialog) {
    this.cartID = localStorage.getItem('currentCart')!
  }

  ngOnInit(): void {}

  agInit(params: ICellRendererParams): void {
    this.params = params;
  }

  editProduct(){
    this.openDialog()
  }

  removeProduct(){
    if(this.params.data){
      this._showRemoveDialog(this.params.data.id)
    }
  }

  private _showRemoveDialog(productID:string){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
          this.productService.deleteProduct(productID)
          Swal.fire(
            'Deleted!',
            'The selected product was removed from product list',
            'success'
          )

      }
    })
  }

  addProductToCart(){
    const productSelected:Product = {...this.params.data}
    this.cartService.getProductDocID(this.cartID, productSelected).pipe(
      take(1)
    ).subscribe(
      resp=>{
        if(resp.length>0){
          this.cartService.updateProductQuantity(resp[0], 1);
        }else{
          this.cartService.createNewProductsCart(this.cartID, productSelected);
        }
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Product added to cart',
          showConfirmButton: false,
          timer: 1000
        })
      }
    )
  }

  refresh() {
    return false;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ProductsFormComponent, {
      width: '350px',
      data: {isOpenForEdit: true, product: this.params.data},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
