import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule } from '@angular/router';
import { ProductActionsComponent } from './product-actions/product-actions.component';
import { MaterialModule } from '../material.module';
import { ProductsFormComponent } from './products-form/products-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { ProductsTableComponent } from './products-table/products-table.component';
import { AgGridModule } from 'ag-grid-angular';
import { CartActionsComponent } from './cart-actions/cart-actions.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export function minlengthValidationMessage(err:any, field:any) {
  return `Should have at least ${field.templateOptions.minLength} characters`;
}

export function maxlengthValidationMessage(err:any, field:any) {
  return `This value should be less than ${field.templateOptions.maxLength} characters`;
}

export function minValidationMessage(err:any, field:any) {
  return `This value should be more than ${field.templateOptions.min}`;
}

export function maxValidationMessage(err:any, field:any) {
  return `This value should be less than ${field.templateOptions.max}`;
}

@NgModule({
  declarations: [
    NavBarComponent,
    ProductActionsComponent,
    ProductsFormComponent,
    ProductsTableComponent,
    CartActionsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      validationMessages: [
        { name: 'required', message: 'This field is required' },
        { name: 'minlength', message: minlengthValidationMessage },
        { name: 'maxlength', message: maxlengthValidationMessage },
        { name: 'min', message: minValidationMessage },
        { name: 'max', message: maxValidationMessage },
      ],
    }),
    FormlyMaterialModule,
    AgGridModule,
  ],
  exports: [
    NavBarComponent,
    ProductsTableComponent,
    CartActionsComponent,
  ]
})
export class ComponentsModule { }
