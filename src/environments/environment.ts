// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'challenge-coa-f0088',
    appId: '1:401387148094:web:eac3b9fb927ced48c2126f',
    storageBucket: 'challenge-coa-f0088.appspot.com',
    locationId: 'southamerica-east1',
    apiKey: 'AIzaSyBjBtn-Ys8BTxr9MJVu8lKvRaL64WYyg8o',
    authDomain: 'challenge-coa-f0088.firebaseapp.com',
    messagingSenderId: '401387148094',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
