# CoaChallenge

## Welcome!
### This is a challenge for "Coa Tecnologia".

**Do you want to download and execute?**
- Clone repository
- Open proyect and run command: `npm install`

**Register**
- If you register on app, go to your mailbox and verify your new account. (Don't forget to check on spam messages)

**What is the proyect about?**

*The proyect is a CRUD system, simulate a e-commerce with Products & Cart*
- Developed with: Angular, Typescript, Formly, AgGrid & NGRX 
- Host & Database: Firebase
---

**Check on:** [challenge-coa](https://challenge-coa-f0088.web.app/)
- email: `maxi.barraza@hotmail.com`
- password: `123456`
